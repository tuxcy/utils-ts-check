import {assert, expect} from "chai";
import {CheckUtils} from "../src/CheckUtils";

describe('CheckUtils', function () {
    beforeEach(function () {
    });

    it('Array checking', function () {
        expect(() => CheckUtils.checkArray([])).to.not.throw();
        expect(() => CheckUtils.checkArray(null)).to.throw("null is not an array");
        expect(() => CheckUtils.checkArray(1)).to.throw("1 is not an array");
        expect(() => CheckUtils.checkArray({})).to.throw("{} is not an array");
        expect(() => CheckUtils.checkArray("tru")).to.throw("\"tru\" is not an array");
        expect(() => CheckUtils.checkArray(true)).to.throw("true is not an array");

        expect(() => CheckUtils.checkIsNotArray([])).to.throw("[] is an array");
        expect(() => CheckUtils.checkIsNotArray(null)).to.not.throw();
        expect(() => CheckUtils.checkIsNotArray(1)).to.not.throw();
        expect(() => CheckUtils.checkIsNotArray({})).to.not.throw();
        expect(() => CheckUtils.checkIsNotArray("tru")).to.not.throw();
        expect(() => CheckUtils.checkIsNotArray(true)).to.not.throw();
    });

    it('Null / Empty checking', function () {
        expect(() => CheckUtils.checkIsNotEmpty([])).to.throw("[] is empty");
        expect(() => CheckUtils.checkIsNotEmpty([0])).to.not.throw();
        expect(() => CheckUtils.checkIsNotEmpty(null)).to.throw("null is empty");
        expect(() => CheckUtils.checkIsNotEmpty(1)).to.throw("1 is empty");
        expect(() => CheckUtils.checkIsNotEmpty({})).to.throw("{} is empty");
        expect(() => CheckUtils.checkIsNotEmpty({a: false})).to.not.throw();
        expect(() => CheckUtils.checkIsNotEmpty("tru")).to.not.throw();
        expect(() => CheckUtils.checkIsNotEmpty(true)).to.throw("true is empty");

        expect(() => CheckUtils.checkIsNotNull([])).to.not.throw();
        expect(() => CheckUtils.checkIsNotNull(null)).to.throw();
        expect(() => CheckUtils.checkIsNotNull(undefined)).to.throw();
        expect(() => CheckUtils.checkIsNotNull(0)).to.not.throw();
        expect(() => CheckUtils.checkIsNotNull({})).to.not.throw();
        expect(() => CheckUtils.checkIsNotNull({a: false})).to.not.throw();
        expect(() => CheckUtils.checkIsNotNull("tru")).to.not.throw();
        expect(() => CheckUtils.checkIsNotNull(true)).to.not.throw();
        expect(() => CheckUtils.checkIsNotNull(false)).to.not.throw();
        expect(() => CheckUtils.checkIsNotNull("")).to.not.throw();
    });

    it('Number checking', function () {
        expect(() => CheckUtils.checkIsNumber([])).to.throw("[] is not a number");
        expect(() => CheckUtils.checkIsNumber(null)).to.throw("null is not a number");
        expect(() => CheckUtils.checkIsNumber(1)).to.not.throw();
        expect(() => CheckUtils.checkIsNumber(-1)).to.not.throw();
        expect(() => CheckUtils.checkIsNumber(-0.33)).to.not.throw();
        expect(() => CheckUtils.checkIsNumber({})).to.throw("{} is not a number");
        expect(() => CheckUtils.checkIsNumber("tru")).to.throw("\"tru\" is not a number");
        expect(() => CheckUtils.checkIsNumber(true)).to.throw("true is not a number");

        expect(() => CheckUtils.checkIsNotNumber([])).to.not.throw();
        expect(() => CheckUtils.checkIsNotNumber(null)).to.not.throw();
        expect(() => CheckUtils.checkIsNotNumber(1)).to.throw("1 is a number");
        expect(() => CheckUtils.checkIsNotNumber(-1)).to.throw("-1 is a number");
        expect(() => CheckUtils.checkIsNotNumber(-0.33)).to.throw("-0.33 is a number");
        expect(() => CheckUtils.checkIsNotNumber({})).to.not.throw();
        expect(() => CheckUtils.checkIsNotNumber("tru")).to.not.throw();
        expect(() => CheckUtils.checkIsNotNumber(true)).to.not.throw();
    });

    it('String checking', function () {
        expect(() => CheckUtils.checkIsString([])).to.throw("[] is not a string");
        expect(() => CheckUtils.checkIsString(null)).to.throw("null is not a string");
        expect(() => CheckUtils.checkIsString(1)).to.throw("1 is not a string");
        expect(() => CheckUtils.checkIsString({})).to.throw("{} is not a string");
        expect(() => CheckUtils.checkIsString("tru")).to.not.throw();
        expect(() => CheckUtils.checkIsString(true)).to.throw("true is not a string");

        expect(() => CheckUtils.checkIsNotString([])).to.not.throw();
        expect(() => CheckUtils.checkIsNotString(null)).to.not.throw();
        expect(() => CheckUtils.checkIsNotString(1)).to.not.throw();
        expect(() => CheckUtils.checkIsNotString({})).to.not.throw();
        expect(() => CheckUtils.checkIsNotString("tru")).to.throw("\"tru\" is a string");
        expect(() => CheckUtils.checkIsNotString(true)).to.not.throw();

        expect(() => CheckUtils.checkIsStringOrNumber([])).to.throw("[] is neither a string or a number");
        expect(() => CheckUtils.checkIsStringOrNumber(null)).to.throw("null is neither a string or a number");
        expect(() => CheckUtils.checkIsStringOrNumber(1)).to.not.throw();
        expect(() => CheckUtils.checkIsStringOrNumber({})).to.throw("{} is neither a string or a number");
        expect(() => CheckUtils.checkIsStringOrNumber("tru")).to.not.throw();
        expect(() => CheckUtils.checkIsStringOrNumber(true)).to.throw("true is neither a string or a number");
    });

    it('check Arguments', function () {
        expect(() => CheckUtils.checkArguments(["a", "b", null], CheckUtils.checkIsNotNull)).to.throw();
    })
});
