import * as _ from 'underscore'

class CheckUtils {
    static checkArguments(args: any[], method) {
        args.forEach((el) => {
            method(el);
        })
    }
    static checkArray(val) {
        if (!Array.isArray(val)) throw new Error(JSON.stringify(val) + " is not an array");
    }
    static checkIsNotArray(val) {
        if (Array.isArray(val)) throw new Error(JSON.stringify(val) + " is an array");
    }
    static checkIsNotNull(val) {
        if (val || val === 0 || val === false || val === "") return;
        throw new Error(JSON.stringify(val) + " is not defined");
    }
    static checkIsNotEmpty(val) {
        if (_.isEmpty(val)) throw new Error(JSON.stringify(val) + " is empty");
    }
    static checkIsString(val) {
        if (!(typeof val === "string")) throw new Error(JSON.stringify(val) + " is not a string");
    }
    static checkIsNotString(val) {
        if (typeof val === "string") throw new Error(JSON.stringify(val) + " is a string");
    }
    static checkIsNumber(val) {
        if (!(typeof val === "number")) throw new Error(JSON.stringify(val) + " is not a number");
    }
    static checkIsNotNumber(val) {
        if (typeof val === "number") throw new Error(JSON.stringify(val) + " is a number");
    }
    static checkIsStringOrNumber(val) {
        let isString: boolean = (typeof val === "string");
        let isNumber: boolean = (typeof val === "number");

        if (!isString && !isNumber) throw new Error(JSON.stringify(val) + " is neither a string or a number");
    }
}

export {
    CheckUtils
}
